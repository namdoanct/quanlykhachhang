<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('adds', function (Blueprint $table) {
            $table->id();
            // gồm các field Mã, Địa chỉ, Điện thoại, Email, Loại khách hàng, Tên, Id/passport, Di dộng, Tài khoản ngân hàng, Hạn thanh toán, Ngày sinh, Ngày cấp, Fax, Tên ngân hàng
            $table->string('ma');
            $table->string('diachi');
            $table->string('dienthoai');
            $table->string('email');
            $table->string('loaikhachhang');
            $table->string('ten');
            $table->string('idpassport');
            $table->string('didong');
            $table->string('taikhoannganhang');
            $table->string('hanthanhtoan');
            $table->date('ngaysinh');
            $table->date('ngaycap');
            $table->string('fax');
            $table->string('tennganhang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('adds');
    }
};
