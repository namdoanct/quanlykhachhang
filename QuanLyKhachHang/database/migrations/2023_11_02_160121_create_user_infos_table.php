<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->id();
            // gồm các field Mã, Địa chỉ, Điện thoại, Email, Loại khách hàng, Tên, Id/passport, Di dộng, Tài khoản ngân hàng, Hạn thanh toán, Ngày sinh, Ngày cấp, Fax, Tên ngân hàng
            $table->string('ma')->nullable();
            $table->string('loaikhachhang')->nullable();
            $table->string('idpassport')->nullable();
            $table->string('taikhoannganhang')->nullable();
            $table->string('hanthanhtoan')->nullable();
            $table->date('ngaycap')->nullable();
            $table->string('fax')->nullable();
            $table->string('tennganhang')->nullable();
            // tạo khóa ngoại với bảng users
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_infos');
    }
};
