<?php

namespace Database\Seeders;

use App\Models\Add;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AddSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Add::factory()->count(10)->create();
    }
}
