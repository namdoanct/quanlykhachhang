<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Add>
 */
class AddFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [

            "ma"=> $this->faker->numberBetween(1,10),
            "diachi"=> $this->faker->text,
            // random số điện thoại với 11 ký tự số
            "dienthoai"=> $this->faker->numberBetween(10000000000,99999999999),
            "email"=> $this->faker->email,
            "loaikhachhang"=> $this->faker->text,
            "ten"=> $this->faker->text,
            "idpassport"=> $this->faker->text,
            "didong"=> $this->faker->text,
            "taikhoannganhang"=> $this->faker->text,
            "hanthanhtoan"=> $this->faker->text,
            "ngaysinh"=> $this->faker->date,
            "ngaycap"=> $this->faker->date,
            "fax"=> $this->faker->text,
            "tennganhang"=> $this->faker->text,
        ];
    }
}



