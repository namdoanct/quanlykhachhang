<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserInfo>
 */
class UserInfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "ma"=> $this->faker->numberBetween(1,10),
            "loaikhachhang"=> $this->faker->text,
            "idpassport"=> $this->faker->text,
            "taikhoannganhang"=> $this->faker->text,
            "hanthanhtoan"=> $this->faker->text,
            "ngaycap"=> $this->faker->date,
            "fax"=> $this->faker->text,
            "tennganhang"=> $this->faker->text,
            "user_id"=> $this->faker->numberBetween(1,10),
        ];
    }
}
