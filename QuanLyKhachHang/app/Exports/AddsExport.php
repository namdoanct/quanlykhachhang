<?php

namespace App\Exports;

use App\Models\Add;
use Maatwebsite\Excel\Concerns\FromCollection;

class AddsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Add::all();
    }
}
