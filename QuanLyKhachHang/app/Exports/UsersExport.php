<?php

namespace App\Exports;

use App\Models\User;
use App\Models\UserInfo;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        // trả về tất cả các bản ghi trong bảng users và bảng user_infos
        $users = User::join('user_infos', 'users.id', '=', 'user_infos.user_id')
            ->select(
                'users.id',
                'users.name',
                'users.diachi',
                'users.dienthoai',
                'users.didong',
                'users.ngaysinh',
                'users.email',
                'user_infos.ma',
                'user_infos.loaikhachhang',
                'user_infos.idpassport',
                'user_infos.taikhoannganhang',
                'user_infos.hanthanhtoan',
                'user_infos.ngaycap',
                'user_infos.fax',
                'user_infos.tennganhang'
            )
            ->get();


        return $users;
    }

    // define header cho file excel
    public function headings(): array
    {
        return [
            'id',
            'name',
            'diachi',
            'dienthoai',
            'didong',
            'ngaysinh',
            'email',
            'ma',
            'loaikhachhang',
            'idpassport',
            'taikhoannganhang',
            'hanthanhtoan',
            'ngaycap',
            'fax',
            'tennganhang',
        ];
    }
}
