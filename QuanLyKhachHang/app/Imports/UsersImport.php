<?php

namespace App\Imports;

use App\Models\User;
use App\Models\UserInfo;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // update or create cho table users va table user_infos

        try {

            $user = User::where("email", $row[6])->first();
            if ($user) {
                $user->update([
                    'name'     => $row[1],
                    'diachi'     => $row[2],
                    'dienthoai'    => $row[3],
                    'didong'     => $row[4],
                    'ngaysinh'    => $row[5],
                ]);
    
                $user->userInfo()->updateOrCreate(
                    [
                        'ma' => $row[7],
                    ],
                    [
                        'loaikhachhang' => $row[8],
                        'idpassport' => $row[9],
                        'taikhoannganhang' => $row[10],
                        'hanthanhtoan' => $row[11],
                        'ngaycap' => $row[12],
                        'fax' => $row[13],
                        'tennganhang' => $row[14],
                    ]
                );
            }else
            {
                $user = User::create([
                    'name'     => $row[1],
                    'diachi'     => $row[2],
                    'dienthoai'    => $row[3],
                    'didong'     => $row[4],
                    'ngaysinh'    => $row[5],
                    'email'    => $row[6],
                    'password' => Hash::make('123456'),
                ]);
    
                $user->userInfo()->create([
                    'ma' => $row[7],
                    'loaikhachhang' => $row[8],
                    'idpassport' => $row[9],
                    'taikhoannganhang' => $row[10],
                    'hanthanhtoan' => $row[11],
                    'ngaycap' => $row[12],
                    'fax' => $row[13],
                    'tennganhang' => $row[14],
                ]);
            }
            
        }catch(Exception $e){
            Log::error($e->getMessage());
        }

    
    }   
}
