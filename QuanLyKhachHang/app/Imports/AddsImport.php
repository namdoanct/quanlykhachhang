<?php

namespace App\Imports;

use App\Models\Add;
use Maatwebsite\Excel\Concerns\ToModel;

class AddsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // return new Add([
        //     'ma'     => $row[1],
        //     'diachi'    => $row[2],
        //     'dienthoai'     => $row[3],
        //     'email'    => $row[4],
        //     'loaikhachhang'     => $row[5],
        //     'ten'    => $row[6],
        //     'idpassport'     => $row[7],
        //     'taikhoannganhang'    => $row[8],
        //     'didong'     => $row[9],
        //     'hanthanhtoan'    => $row[10],
        //     'ngaysinh'     => $row[11],
        //     'ngaycap'    => $row[12],
        //     'fax'     => $row[13],
        //     'tennganhang'    => $row[14],
        // ]);

        // update or create
        return Add::updateOrCreate([
            'ma'     => $row[1],
            'diachi'    => $row[2],
            'dienthoai'     => $row[3],
            'email'    => $row[4],
            'loaikhachhang'     => $row[5],
            'ten'    => $row[6],
            'idpassport'     => $row[7],
            'taikhoannganhang'    => $row[8],
            'didong'     => $row[9],
            'hanthanhtoan'    => $row[10],
            'ngaysinh'     => $row[11],
            'ngaycap'    => $row[12],
            'fax'     => $row[13],
            'tennganhang'    => $row[14],
        ]);


    }
}
