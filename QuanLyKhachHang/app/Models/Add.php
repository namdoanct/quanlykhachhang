<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Add extends Model
{
    use HasFactory;

    protected $fillable = [
        "ma",
        "diachi",
        "dienthoai",
        "email",
        "loaikhachhang",
        "ten",
        "idpassport",
        "didong",
        "taikhoannganhang",
        "hanthanhtoan",
        "ngaysinh",
        "ngaycap",
        "fax",
        "tennganhang"
        ];
}
