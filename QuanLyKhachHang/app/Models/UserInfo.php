<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        "ma",
        "loaikhachhang",
        "idpassport",
        "taikhoannganhang",
        "hanthanhtoan",
        "ngaycap",
        "fax",
        "tennganhang",
        "user_id",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
