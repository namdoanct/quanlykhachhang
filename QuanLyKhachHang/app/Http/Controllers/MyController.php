<?php

namespace App\Http\Controllers;

use App\Exports\AddsExport;
use App\Exports\UsersExport;
use App\Imports\AddsImport;
use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Maatwebsite\Excel\Facades\Excel;

class MyController extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExportView()
    {

        return view('import');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new UsersExport, 'adds.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import()
    {
        Excel::import(new UsersImport, request()->file('file'));

        return back();
    }

    public function store(Request $request)
    {
        // lưu thông tin của khách hàng vào bảng user và bảng user_info
        $this->validate($request, [
            'ma' => 'required',
            'diachi' => 'required',
            'dienthoai' => 'required',
            'email' => 'required',
            'loaikhachhang' => 'required',
            'ten' => 'required',
            'idpassport' => 'required',
            'didong' => 'required',
            'taikhoannganhang' => 'required',
            'hanthanhtoan' => 'required',
            'ngaysinh' => 'required',
            'ngaycap' => 'required',
            'fax' => 'required',
            'tennganhang' => 'required',
        ]);

        // lưu thông tin của khách hàng vào model user
        $user = new \App\Models\User;
        $user->name = $request->input('ten');
        $user->diachi = $request->input('diachi');
        $user->dienthoai = $request->input('dienthoai');
        $user->didong = $request->input('didong');
        $user->ngaysinh = $request->input('ngaysinh');
        $user->email = $request->input('email');
        $user->password = Hash::make('password');
        $user->save();

        // lưu thông tin của khách hàng vào model user_info

        $user_info = new \App\Models\UserInfo;
        $user_info->ma = $request->input('ma');
        $user_info->loaikhachhang = $request->input('loaikhachhang');
        $user_info->idpassport = $request->input('idpassport');
        $user_info->taikhoannganhang = $request->input('taikhoannganhang');
        $user_info->hanthanhtoan = $request->input('hanthanhtoan');
        $user_info->ngaycap = $request->input('ngaycap');
        $user_info->fax = $request->input('fax');
        $user_info->tennganhang = $request->input('tennganhang');
        $user_info->user_id = $user->id;
        $user_info->save();

        return redirect('/importExportView')->with('success', 'Thêm khách hàng thành công');
    }

    // hiển thị tất cả các khách hàng
    public function index()
    {
            // hiển thị tất cả các khách hàng từ bảng user và user_infos
            $users = \App\Models\User::join('user_infos', 'users.id', '=', 'user_infos.user_id')
                ->select('users.*', 'user_infos.*')
                ->get();


                return view('/import', compact('users'));

    }
}
