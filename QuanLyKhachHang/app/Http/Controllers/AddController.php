<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AddController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        // 
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // nhập thông tin của khách hàng
        return view('add.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // lưu thông tin của khách hàng vào bảng user và bảng user_info
        $this->validate($request, [
            'ma'=> 'required',
            'diachi'=> 'required',
            'dienthoai'=> 'required',
            'email'=> 'required',
            'loaikhachhang'=> 'required',
            'ten'=> 'required',
            'idpassport'=> 'required',
            'didong'=> 'required',
            'taikhoannganhang'=> 'required',
            'hanthanhtoan'=> 'required',
            'ngaysinh'=> 'required',
            'ngaycap'=> 'required',
            'fax'=> 'required',
            'tennganhang'=> 'required',
        ]);

        // lưu thông tin của khách hàng vào model user
        $user = new \App\Models\User;
        $user->name = $request->input('ten');
        $user->diachi = $request->input('diachi');
        $user->dienthoai = $request->input('dienthoai');
        $user->didong = $request->input('didong');
        $user->ngaysinh = $request->input('ngaysinh');
        $user->email = $request->input('email');
        $user->password = Hash::make('password');
        $user->save();

        // lưu thông tin của khách hàng vào model user_info

        $user_info = new \App\Models\UserInfo;
        $user_info->ma = $request->input('ma');
        $user_info->loaikhachhang = $request->input('loaikhachhang');
        $user_info->idpassport = $request->input('idpassport');
        $user_info->taikhoannganhang = $request->input('taikhoannganhang');
        $user_info->hanthanhtoan = $request->input('hanthanhtoan');
        $user_info->ngaycap = $request->input('ngaycap');
        $user_info->fax = $request->input('fax');
        $user_info->tennganhang = $request->input('tennganhang');
        $user_info->user_id = $user->id;
        $user_info->save();

        return redirect('/')->with('success', 'Thêm khách hàng thành công');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
