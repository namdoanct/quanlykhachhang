<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;

class Usercontroller extends Controller
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExportView()
    {

        return view('users/user');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function import()
    {
        Excel::import(new UsersImport, request()->file('file'));

        // hiển thị thông báo thành công


        return back();
    }

    public function store(Request $request)
    {
        // validate thông tin của khách hàng
        $this->validate($request, [
            'name' => 'required',
            'didong' => 'required',
        ]);

        // lưu thông tin của khách hàng vào model user
        $user = new User;
        $user->name = $request->input('name');
        $user->diachi = $request->input('diachi');
        $user->dienthoai = $request->input('dienthoai');
        $user->didong = $request->input('didong');
        $user->ngaysinh = $request->input('ngaysinh');
        $user->email = $request->input('email');
        $user->password = Hash::make('password');
        $user->save();


        // lưu thông tin của khách hàng vào model user_info
        $user_info = new \App\Models\UserInfo;
        // tăng giá trị của mã GD001 lên 1 
        $user_info->ma = 'GD' . str_pad($user->id, 3, '0', STR_PAD_LEFT);
        $user_info->ma = $request->input('ma');
        $user_info->loaikhachhang = $request->input('loaikhachhang');
        $user_info->idpassport = $request->input('idpassport');
        $user_info->taikhoannganhang = $request->input('taikhoannganhang');
        $user_info->hanthanhtoan = $request->input('hanthanhtoan');
        $user_info->ngaycap = $request->input('ngaycap');
        $user_info->fax = $request->input('fax');
        $user_info->tennganhang = $request->input('tennganhang');
        $user_info->user_id = $user->id;
        $user_info->save();




        return redirect('/user-management')->with('success', 'Thêm khách hàng thành công');
    }

    // hiển thị tất cả các khách hàng
    public function index()
    {
        // hiển thị tất cả các khách hàng từ bảng user và user_infos
        $users = User::join('user_infos', 'users.id', '=', 'user_infos.user_id')
            ->select('users.*', 'user_infos.*')
            ->get();


        return view('users/user', compact('users'));
    }

    public function show(string $id)
    {
        // hiển thị chi tiết của khách hàng từ bảng user và user_infos
        $user = User::join('user_infos', 'users.id', '=', 'user_infos.user_id')
            ->select('users.*', 'user_infos.*')
            ->findOrFail($id);


        return view('users/detail', compact('user'));
    }
}
