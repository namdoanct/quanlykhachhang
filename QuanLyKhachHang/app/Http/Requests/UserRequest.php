<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'ma' => 'required',
            'diachi' => 'required',
            'dienthoai' => 'required',
            'email' => 'required',
            'loaikhachhang' => 'required',
            'ten' => 'required',
            'idpassport' => 'required',
            'didong' => 'required',
            'taikhoannganhang' => 'required',
            'hanthanhtoan' => 'required',
            'ngaysinh' => 'required',
            'ngaycap' => 'required',
            'fax' => 'required',
            'tennganhang' => 'required'
        ];
    }
}
