<?php

use App\Http\Controllers\AddController;
use App\Http\Controllers\MyController;
use App\Http\Controllers\Usercontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// khởi tạo route get của addcontroller

// Route::get('/add/create', [AddController::class, 'create'])->name('add.create');

// Route::post('/add/store', [AddController::class, 'store'])->name('add.store');

Route::get('/export', [Usercontroller::class, 'export'])->name('export');
// Route::get('importExportView', 'MyController@importExportView');
Route::get('/user-management', [Usercontroller::class, 'importExportView'])->name('user-management');
Route::post('/import', [Usercontroller::class, 'import'])->name('import');

Route::post('/store', [Usercontroller::class, 'store'])->name('import.store');

// route hiển thị danh sách khách hàng
Route::get('/user-management', [Usercontroller::class, 'index'])->name('user-management.index');

// route hiển thị chi tiết khách hàng
Route::get('/user-management/{id}', [Usercontroller::class, 'show'])->name('user-management.show');