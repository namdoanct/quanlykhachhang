<!-- 
<form method="POST" action="/store">
    @csrf

    <div class="form-group">
        <label for="name">Mã</label>
        <input type="text" name="ma" />
        <label for="name">Địa chỉ</label>
        <input type="text" name="diachi" />
        <label for="name">Điện thoại</label>
        <input type="text" name="dienthoai" />
        <label for="name">Email</label>
        <input type="text" name="email" />
        <label for="name">Loại khách hàng</label>
        <input type="text" name="loaikhachhang" />
        <label for="name">Tên</label>
        <input type="text" name="ten" />
        <label for="name">Id/passport</label>
        <input type="text" name="idpassport" />
        <label for="name">Di dộng</label>
        <input type="text" name="didong" />
        <label for="name">Tài khoản ngân hàng</label>
        <input type="text" name="taikhoannganhang" />
        <label for="name">Hạn thanh toán</label>
        <input type="text" name="hanthanhtoan" />
        <label for="name">Ngày sinh</label>
        <input type="text" name="ngaysinh" />
        <label for="name">Ngày cấp</label>
        <input type="text" name="ngaycap" />
        <label for="name">Fax</label>
        <input type="text" name="fax" />
        <label for="name">Tên ngân hàng</label>
        <input type="text" name="tennganhang" />

        <button type="submit">Submit</button>
    </div>
</form> -->

<form action="{{ route('add.store') }}" method="post">
    @csrf

    <div class="form-group">
        <div class="add-one">
            <label for="name">Mã</label>
            <input type="text" name="ma" />
            <label for="name">Địa chỉ</label>
            <input type="text" name="diachi" />
            <label for="name">Điện thoại</label>
            <input type="number" name="dienthoai" />
            <label for="name">Email</label>
            <input type="email" name="email" />
            <label for="name">Loại khách hàng</label>
            <!-- tạo lựa chọn cho loại khách hàng -->
            <select name="loaikhachhang">
                <option value="Khác">Khác</option>
                <option value="Là Khách Hàng">Là Khách Hàng</option>
                <option value="Không Phải Khách Hàng">Không Phải Khách Hàng</option>
            </select>
        </div>
        <div class="add-two">
            <label for="name">Tên</label>
            <input type="text" name="ten" />
            <label for="name">Id/passport</label>
            <input type="number" name="idpassport" />
            <label for="name">Di dộng</label>
            <input type="text" name="didong" />
            <label for="name">Tài khoản ngân hàng</label>
            <input type="number" name="taikhoannganhang" />
            <label for="name">Hạn thanh toán</label>
            <input type="text" name="hanthanhtoan" />
            <div class="save-quit">
                <a href="" class="btn btn-secondary"><i class="fas fa-long-arrow-alt-left"></i> <span class="ml-2">{{ __('Thoát') }}</span></a>
                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> <span class="ml-2">{{ __('Thêm mới') }}</span></button>
            </div>
        </div>
        <div class="add-three">
            <!-- field ngày sinh là gì -->
            <label for="name">Ngày sinh</label>
            <input type="date" name="ngaysinh" />
            <label for="name">Ngày cấp</label>
            <input type="date" name="ngaycap" />
            <label for="name">Fax</label>
            <input type="text" name="fax" />
            <label for="name">Tên ngân hàng</label>
            <input type="text" name="tennganhang" />
        </div>

    </div>


</form>

<!-- css cho form và các phần tử bên trong -->
<style>
    .add-one {
        float: left;
        width: 33%;
    }

    .add-two {
        float: left;
        width: 33%;
    }

    .add-three {
        float: left;
        width: 33%;
    }

    .add-one label {
        display: block;
        margin-bottom: 5px;
    }

    .add-two label {
        display: block;
        margin-bottom: 5px;
    }


    .add-three label {
        display: block;
        margin-bottom: 5px;
    }


    .add-one input {
        width: 100%;
        padding: 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
    }

    .add-two input {
        width: 100%;
        padding: 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
    }


    .add-three input {
        width: 100%;
        padding: 5px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin-bottom: 10px;
    }


    .add-one button {
        padding: 10px 20px;
        border: 1px solid #ccc;
        border-radius: 3px;
        background: #fff;
        cursor: pointer;
    }


    .add-two button {
        padding: 10px 20px;
        border: 1px solid #ccc;
        border-radius: 3px;
        background: #fff;
        cursor: pointer;
    }


    .add-three button {
        padding: 10px 20px;
        border: 1px solid #ccc;
        border-radius: 3px;
        background: #fff;
        cursor: pointer;
    }


    .add-one button:hover {
        background: #f5f5f5;
    }


    .add-two button:hover {
        background: #f5f5f5;
    }


    .add-three button:hover {
        background: #f5f5f5;
    }


    .add-one button:active {
        background: #f5f5f5;
    }


    .add-two button:active {
        background: #f5f5f5;
    }


    .add-three button:active {
        background: #f5f5f5;
    }
</style>