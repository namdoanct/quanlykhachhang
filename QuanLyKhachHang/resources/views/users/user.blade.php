@extends('layouts.master')

{{-- css --}}
@include('layouts.css')

@section('content')

<body>
    <div class="tab">
        <button class="tablinks" onclick="openCity(event, 'Inputs')">Nhập liệu</button>
        <button class="tablinks" onclick="openCity(event, 'Import')">Import</button>
        <button class="tablinks" onclick="openCity(event, 'Export')">Export</button>
    </div>


    <div id="Inputs" class="tabcontent">
        <div class="container">
            <form action="{{ route('import.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <div class="add-one">
                        <div class="field">
                            <label for="name">Mã</label>
                            <input type="text" name="ma" value="{{ old('ma', $user_info->ma ?? '') }}" placeholder="GD001">
                        </div>
                        <!-- @error('ma')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror -->

                        <div class="field">
                            <label for="name">Địa chỉ</label>
                            <input type="text" name="diachi" />
                        </div>

                        <div class="field">
                            <label for="name">Điện thoại</label>
                            <input type="number" name="dienthoai" />
                        </div>
                        <!-- @error('dienthoai')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror -->

                        <div class="field">
                            <label for="name">Email</label>
                            <input type="email" name="email" />
                        </div>
                        <div class="field">
                            <label for="name">Loại khách hàng</label>
                            <!-- tạo lựa chọn cho loại khách hàng -->
                            <select name="loaikhachhang">
                                <option value="Khác">Khác</option>
                                <option value="Là Khách Hàng">Là Khách Hàng</option>
                                <option value="Không Phải Khách Hàng">Không Phải Khách Hàng</option>
                            </select>
                        </div>
                    </div>

                    <div class="add-two">

                        <div class="field">
                            <label for="name">Tên <strong>*</strong></label>
                            <input type="text" name="name" />
                        </div>
                        @error('name')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror

                        <div class="field">
                            <label for="name">Id/passport</label>
                            <input type="number" name="idpassport" />
                        </div>

                        <div class="field">
                            <label for="name">Di dộng <strong>*</strong></label>
                            <input type="number" name="didong" min="10" />
                        </div>
                        @error('didong')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror

                        <div class="field">
                            <label for="name">Tài khoản ngân hàng</label>
                            <input type="number" name="taikhoannganhang" />
                        </div>

                        <div class="field">
                            <label for="name">Hạn thanh toán</label>
                            <input type="text" name="hanthanhtoan" />
                        </div>

                        <div class="save-quit">
                            <a href="" class="btn btn-secondary"><i class="fas fa-long-arrow-alt-left"></i> <span class="ml-2">{{ __('Thoát') }}</span></a>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> <span class="ml-2">{{ __('Thêm mới') }}</span></button>
                        </div>
                    </div>

                    <div class="add-three">
                        <div class="field">
                            <!-- field ngày sinh là gì -->
                            <label for="name">Ngày sinh</label>
                            <input type="date" name="ngaysinh" />
                        </div>

                        <div class="field">

                            <label for="name">Ngày cấp</label>
                            <input type="date" name="ngaycap" />
                        </div>

                        <div class="field">
                            <label for="name">Fax</label>
                            <input type="number" name="fax" />
                        </div>

                        <div class="field">
                            <label for="name">Tên ngân hàng</label>
                            <input type="text" name="tennganhang" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div id="Import" class="tabcontent">
        <div class="container">
            <div class="card bg-light mt-3">
                <div class="card-header">
                    Import Export Excel
                </div>
                <div class="card-body">
                    <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="file" name="file" class="form-control">
                        <br>
                        <button class="btn btn-success">Import User Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div id="Export" class="tabcontent">
        <div class="container">
            <div class="card bg-light mt-3">
                <!-- hiển thị danh sách khách hàng -->
                <div class="card-header">
                    <div class="card-body">
                        <div>Danh sách khách hàng</div>
                        <div class="export-button">
                            <a class="btn btn-warning" href="{{ route('export') }}">Export User Data</a>

                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Tên</th>
                            <th>Địa chỉ</th>
                            <th>Điện thoại</th>
                            <th>Di động</th>
                            <th>Email</th>
                            <th>Loại khách hàng</th>
                            <th>Mã</th>
                            <th>Id/passport</th>
                            <th>
                                <action>Thao tác</action>
                            </th>
                        </tr>
                        @foreach($users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->diachi }}</td>
                            <td>{{ $user->dienthoai }}</td>
                            <td>{{ $user->didong }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->loaikhachhang }}</td>
                            <td>{{ $user->ma }}</td>
                            <td>{{ $user->idpassport }}</td>
                            <td>
                                <a href="{{ route('user-management.show', $user->id) }}" class="btn btn-primary">Chi tiết</a>
                        </tr>
                        @endforeach
                    </table>

                </div>
            </div>
        </div>

</body>

@endsection