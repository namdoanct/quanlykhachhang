@extends('layouts.master')

{{-- css --}}
@include('layouts.css')

@section('content')

 <div class="container">
     <div class="row">
         <div class="col-md-16">
             <div class="card">
                 <div class="card-header">
                     <h4>Chi tiết khách hàng</h4>
                 </div>
                 <div class="card-body">
                     <div class="row">
                         <div class="col-md-6">
                             <h4>Thông tin khách hàng</h4>
                             <table class="table table-bordered">
                                 <thead>
                                     <tr>
                                         <th>Mã</th>
                                         <th>Tên</th>
                                         <th>Địa chỉ</th>
                                         <th>Điện thoại</th>
                                         <th>Di động</th>
                                         <th>Ngày sinh</th>
                                         <th>Email</th>
                                         <th>Loại khách hàng</th>
                                         <th>ID/Passport</th>
                                         <th>Tài khoản ngân hàng</th>
                                     </tr>
                                 </thead>
                                 <tbody>
                                     <tr>
                                         <td>{{ $user->ma }}</td>
                                         <td>{{ $user->name }}</td>
                                         <td>{{ $user->diachi }}</td>
                                         <td>{{ $user->dienthoai }}</td>
                                         <td>{{ $user->didong }}</td>
                                         <td>{{ $user->ngaysinh }}</td>
                                         <td>{{ $user->email }}</td>
                                         <td>{{ $user->loaikhachhang }}</td>
                                         <td>{{ $user->idpassport }}</td>
                                         <td>{{ $user->taikhoannganhang }}</td>
                                     </tr>
                                 </tbody>
                             </table>
                             <!-- viết button trả về trang user-view -->
                                <a href="{{ route('user-management.index') }}" class="btn btn-primary">Trở về</a>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>


 @endsection