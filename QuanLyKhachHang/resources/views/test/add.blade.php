// tạo ra form nhập các thông tin sau: Mã, Địa chỉ, Điện thoại, Email, Loại khách hàng, Tên, Id/passport, Di dộng, Tài khoản ngân hàng, Hạn thanh toán, Ngày sinh, Ngày cấp, Fax, Tên ngân hàng

<form method="POST" action="/example">
    @csrf

    <div class="form-group">
        <label for="name">Mã</label>
        <input type="text" name="ma" />
        <label for="name">Địa chỉ</label>
        <input type="text" name="diachi" />
        <label for="name">Điện thoại</label>
        <input type="text" name="dienthoai" />
        <label for="name">Email</label>
        <input type="text" name="email" />
        <label for="name">Loại khách hàng</label>
        <input type="text" name="loaikhachhang" />
        <label for="name">Tên</label>
        <input type="text" name="ten" />
        <label for="name">Id/passport</label>
        <input type="text" name="idpassport" />
        <label for="name">Di dộng</label>
        <input type="text" name="didong" />
        <label for="name">Tài khoản ngân hàng</label>
        <input type="text" name="taikhoannganhang" />
        <label for="name">Hạn thanh toán</label>
        <input type="text" name="hanthanhtoan" />
        <label for="name">Ngày sinh</label>
        <input type="text" name="ngaysinh" />
        <label for="name">Ngày cấp</label>
        <input type="text" name="ngaycap" />
        <label for="name">Fax</label>
        <input type="text" name="fax" />
        <label for="name">Tên ngân hàng</label>
        <input type="text" name="tennganhang" />
    </div>
</form>