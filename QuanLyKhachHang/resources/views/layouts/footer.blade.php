<div b-dvmk4yi2zk="" class="container-fluid  text-light footer wow fadeIn" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeIn; background-color: #262831;">
            <div b-dvmk4yi2zk="" class="container py-5 px-lg-5">
                <div b-dvmk4yi2zk="" class="row g-5">
                    <div b-dvmk4yi2zk="" class="col-md-6 col-lg-4" id="footer">
                        <p class="section-title text-white h5 mb-4 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">ĐỊA CHỈ <span></span></p>
                        <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fa fa-map-marker-alt me-2 text-info"></i>40 Mỹ Khê 7, Phước Mỹ, Sơn Trà</p>
                        <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fa fa-phone-alt me-2 text-info"></i> 0787616118</p>
                        <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fa fa-map-marker-alt me-2 text-info"></i>support@gardensoft.com.vn</p>
        </div>
                    <div b-dvmk4yi2zk="" class="col-md-6 col-lg-4" id="footerSecond">
                       <p class="section-title text-white h5 mb-4 wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">SẢN PHẨM<span></span></p>
                       <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fas fa-angle-right me-2 text-info wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;"></i>Cung cấp server toàn quốc</p>
                       <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fas fa-angle-right me-2 text-info wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;"></i> Giải pháp mạng</p>
                       <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fas fa-angle-right me-2 text-info wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;"></i>Bảo trì chăm sóc hệ thống</p>
                       <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fas fa-angle-right me-2 text-info wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;"></i>Cung cấp server toàn quốc</p>
                       <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fas fa-angle-right me-2 text-info wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;"></i> Giải pháp mạng</p>
                       <p class="wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;"><i class="fas fa-angle-right me-2 text-info wow fadeInUp" data-wow-delay="0.7s" style="visibility: visible; animation-delay: 0.7s; animation-name: fadeInUp;"></i>Bảo trì chăm sóc hệ thống</p>
        </div>
                    <div b-dvmk4yi2zk="" class="col-md-6 col-lg-4">
                        <p b-dvmk4yi2zk="" class="section-title text-white h5 mb-4">Kết nối với chúng tôi<span b-dvmk4yi2zk=""></span></p>
                        <div b-dvmk4yi2zk="" class="fb-page fb_iframe_widget" data-href="https://www.facebook.com/PhanmemGhotel/?ref=embed_page" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="adapt_container_width=true&amp;app_id=&amp;container_width=324&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FPhanmemGhotel%2F%3Fref%3Dembed_page&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=&amp;width="><span style="vertical-align: bottom; width: 324px; height: 130px;"><iframe name="f326568be258d5" width="1000px" height="1000px" data-testid="fb:page Facebook Social Plugin" title="fb:page Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v17.0/plugins/page.php?adapt_container_width=true&amp;app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Dfa671f18338968%26domain%3Dgardensoft.com.vn%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fgardensoft.com.vn%252Ff2710a7b6cd6824%26relation%3Dparent.parent&amp;container_width=324&amp;hide_cover=false&amp;href=https%3A%2F%2Fwww.facebook.com%2FPhanmemGhotel%2F%3Fref%3Dembed_page&amp;locale=en_US&amp;sdk=joey&amp;show_facepile=true&amp;small_header=false&amp;tabs=&amp;width=" style="border: none; visibility: visible; width: 324px; height: 130px;" class=""></iframe></span></div>
                    </div>
                </div>
            </div>
            <div b-dvmk4yi2zk="" class="container px-lg-5">
                <div b-dvmk4yi2zk="" class="copyright">
                    <div b-dvmk4yi2zk="" class="row">
                        <div b-dvmk4yi2zk="" class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                            © <a b-dvmk4yi2zk="" class="border-bottom" href="#"> 2017 Gardensoft Company Limited</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>