<nav b-dvmk4yi2zk="" class="navbar navbar-expand-lg navbar-light px-4 px-lg-5 py-3 py-lg-0">
    <a b-dvmk4yi2zk="" href="/" class="navbar-brand p-0">
        <h1 b-dvmk4yi2zk="" class="m-0">
            <img b-dvmk4yi2zk="" src="https://gardensoft.com.vn/img/logo.png" alt="Logo Gardensoft" style="max-height:90px">
        </h1>
    </a>
    <button b-dvmk4yi2zk="" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
        <span b-dvmk4yi2zk="" class="fa fa-bars"></span>
    </button>
    <div b-dvmk4yi2zk="" class="collapse navbar-collapse" id="navbarCollapse">
        <div b-dvmk4yi2zk="" class="navbar-nav mx-auto py-0" id="menuBar">
            <div class="nav-item dropdown">
                <a href="/" class="nav-link nav-item ">Trang chủ</a>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle " data-bs-toggle="dropdown">Phần mềm</a>
                <div class="dropdown-menu m-0">
                    <a href="/ghotel" class="dropdown-item">Phần mềm quản lý khách sạn G-hotel.vn</a>

                    <a href="/write-software" class="dropdown-item">Viết phần mềm theo yêu cầu</a>

                    <a href="/digital" class="dropdown-item">Chuyển đổi số theo doanh nghiệp</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle " data-bs-toggle="dropdown">Cloud Hosting</a>
                <div class="dropdown-menu m-0">
                    <a href="/cloud-hosting-linux" class="dropdown-item">Cloud Hosting Linux</a>

                    <a href="/cloud-hosting-windows" class="dropdown-item">Cloud Hosting Windows</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle " data-bs-toggle="dropdown">Email</a>
                <div class="dropdown-menu m-0">
                    <a href="/mail-hosting" class="dropdown-item">Email Hosting</a>

                    <a href="/mail-server" class="dropdown-item">Email Server</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle " data-bs-toggle="dropdown">Network</a>
                <div class="dropdown-menu m-0">
                    <a href="/system" class="dropdown-item">Hệ thống mạng doanh nghiệp</a>

                    <a href="/device" class="dropdown-item">Thiết bị mạng</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle " data-bs-toggle="dropdown">Server (VPS)</a>
                <div class="dropdown-menu m-0">
                    <a href="/rent-place" class="dropdown-item">Cho thuê chỗ đặt Server</a>

                    <a href="/cloud-server" class="dropdown-item">Cloud Server</a>

                    <a href="/build-pc" class="dropdown-item">Build PC</a>

                    <a href="/build-server" class="dropdown-item">Build Server</a>
                </div>
            </div>
            <div class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle " data-bs-toggle="dropdown">Về chúng tôi</a>
                <div class="dropdown-menu m-0">
                    <a href="/contact" class="dropdown-item">Liên hệ</a>

                    <a href="/blog" class="dropdown-item">Hoạt động</a>

                    <a href="/chart" class="dropdown-item">Sơ đồ tổ chức công ty</a>
                </div>
            </div>
        </div>
    </div>
</nav>